#include "sqlite3.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std; //satan

#define TRIES 12

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int getBuyerBalance(void* notUsed, int argc, char** argv, char** azCol);
int getPriceAndAvailable(void* notUsed, int argc, char** argv, char** azCol);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

//global variables//
//For A
int buyerBalance;
int carPrice;
int carAvailable;
//For B
int sellerBalance;

int getBuyerBalance(void* notUsed, int argc, char** argv, char** azCol) // the callback function from the second sql request
{
	std::cout << "The buyer balance is " << argv[0] << std::endl;
	buyerBalance = atoi(argv[0]);
	return 0;
}

int getPriceAndAvailable(void* notUsed, int argc, char** argv, char** azCol)// the callback function from the first sql request
{
	std::cout << "The car price is " << argv[0] << std::endl;
	std::cout << "The car is/isnt available: " << argv[1] << std::endl;
	carPrice = atoi(argv[0]);
	carAvailable = atoi(argv[1]);
	return 0;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg) //B
{
	int rc2;
	int fromBalance, toBalance;
	rc2 = sqlite3_exec(db, "begin transaction;", NULL, 0, &zErrMsg);
	if (rc2 != SQLITE_OK) // if something is wrong with the transference
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	string * firstReq = new string("select balance from accounts where id=" + std::to_string(from) + ";");
	string * secondReq = new string("select balance from accounts where id=" + std::to_string(to) + ";");
	rc2 = sqlite3_exec(db, firstReq->c_str(), getBuyerBalance, 0, &zErrMsg);
	if (rc2 != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	fromBalance = buyerBalance;
	rc2 = sqlite3_exec(db, secondReq->c_str(), getBuyerBalance, 0, &zErrMsg);
	if (rc2 != SQLITE_OK) // if something is wrong with the transference
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	toBalance = buyerBalance;
	if (fromBalance < amount)
	{
		return false; //there isnt enough money to transfer
	}
	fromBalance = fromBalance - amount;
	toBalance = toBalance + amount;
	string * thirdReq = new string("update accounts set balance=" + std::to_string(fromBalance) + " where id=" + std::to_string(from) + ";");
	string * forthReq = new string("update accounts set balance=" + std::to_string(toBalance) + " where id=" + std::to_string(to) + ";");
	rc2 = sqlite3_exec(db, thirdReq->c_str(), getBuyerBalance, 0, &zErrMsg);
	if (rc2 != SQLITE_OK) // if something is wrong with the transference
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	rc2 = sqlite3_exec(db, forthReq->c_str(), getBuyerBalance, 0, &zErrMsg);
	if (rc2 != SQLITE_OK) // if something is wrong with the transference
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	rc2 = sqlite3_exec(db, "commit;", getBuyerBalance, 0, &zErrMsg);
	if (rc2 != SQLITE_OK) // if something is wrong with the transference
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	return true;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg) // A
{
	int rc1;
	string * firstReq = new string("select price,available from cars where id=" + std::to_string(carid) + ";");
	string * secondReq = new string("select balance from accounts where Buyer_id=" + std::to_string(buyerid) + ";");
	rc1 = sqlite3_exec(db, firstReq->c_str(), getPriceAndAvailable, 0, &zErrMsg);
	rc1 = sqlite3_exec(db, secondReq->c_str(), getBuyerBalance, 0, &zErrMsg);

	if (carAvailable == 0)
	{
		return false;
	}
	else if (carPrice > buyerBalance)
	{
		return false;
	}
	else
	{
		int newBalance = buyerBalance - carPrice;
		string * thirdReq = new string("update accounts set balance=" + std::to_string(newBalance) + " where id=" + std::to_string(buyerid) + ";");
		string * forthReq = new string("update cars set available=0 where id=" + std::to_string(carid) + ";");
		rc1 = sqlite3_exec(db, thirdReq->c_str(), NULL, 0, &zErrMsg);
		rc1 = sqlite3_exec(db, forthReq->c_str(), NULL, 0, &zErrMsg);
		return true;
	}
}

void main()
{
	int rc;
	sqlite3* db = 0;
	char *zErrMsg = 0;
	bool success = 0;
	bool successTransference = 0;
	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	success = carPurchase(12, 12, db, zErrMsg);
	if (success == false)
	{
		std::cout << "Error: not Available or Buyer doesnt has enough money!" << std::endl;
	}

	success = carPurchase(5, 22, db, zErrMsg);
	if (success == false)
	{
		std::cout << "Error: not Available or Buyer doesnt has enough money!" << std::endl;
	}

	success = carPurchase(1, 18, db, zErrMsg);
	if (success == false)
	{
		std::cout << "Error: not Available or Buyer doesnt has enough money!" << std::endl;
	}
	
	successTransference = balanceTransfer(7, 11, 50000, db, zErrMsg);
	if (successTransference == false)
	{
		std::cout << "Error with the transference" << std::endl;
	}

	system("pause");
}